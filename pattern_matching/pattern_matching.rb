class PatternMatching
  class << self
    def match_pattern?(pattern, string, pat_map=nil)
      pat_map ||= {}

      return true if string.empty? && pattern.empty?
      return false if string.empty? || pattern.empty?

      # if the pattern character exists
      if pat_map.has_key?(pattern[0])
        chars = pat_map[pattern[0]]

        # check if we can use it to match
        if chars == string[0...chars.length]
          return match_pattern?(pattern[1..-1], string[chars.length..-1], pat_map)
        else
          pat_map.delete(pattern[0])
          return false
        end
      else
        # pattern character does not exist in the map
        (0..[(string.length - pattern.length - 1), 0].max).each do |i|
          pat_map[pattern[0]] = string[0..i]

          return true if match_pattern?(pattern[1..-1], string[i+1..-1], pat_map)
        end
      end
      # we've tried our best but still no luck
      return false
    end
  end
end
